# Vytvorte objekt, ktory bude obsahovat parametre kvadratickej rovnice [korene, diskriminant] 
# ak kv. rovnica nema riesenie, program to vypise. Na koniec vypiste diskriminant a korene rovnice
from math import *
class rovnica:
	def __init__(self, a, b, c):
		self.a = a
		self.b = b
		self.c = c
		self.d = pow(b, 2) - (4*a*c)
		self.check = self.check()
		self.x1 = (-b + sqrt(pow(b, 2) - (4*a*c))) / (2*a)
		self.x2 = (-b - sqrt(pow(b, 2) - (4*a*c))) / (2*a)
	def check(self):
		if(self.d < 0):
			print 'Rovnica nema riesenie'
			exit()

rovnica = rovnica(2,2,2)
print rovnica.d
print rovnica.x1
print rovnica.x2