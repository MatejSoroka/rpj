# Vytvorte objekt, ktory nacita pocet predmetov.
# Potom nacita vsetky nazvy a ich znamky a rozlozi do slovnika ktory posle objektu
# Vytvorte funkcie v objekte, ktore mozme pouzivat po zadani znamok Napr. vypocet priemeru.

from math import *
class prospech:
	def __init__(self, dictionary, count):
		self.dictionary = dictionary
		self.count = count

	def getAverage(self):
		count = 0
		for key in self.dictionary:
			count += int(self.dictionary[key])
		return float(count) / self.count

	def getMarkByName(self, name):
		return self.dictionary[name]

	def getMarkByValue(self, mark):
		array = []
		for key in self.dictionary:
			if int(self.dictionary[key]) == mark:
				array.append(key)
		return array

dictionary = {}
count = int(input('Zadajte pocet znamok: '))
for i in range(1, count + 1):
	subject = str(raw_input('Zadajte skratku predmetu: '))
	value = str(raw_input('Zadajte znamku predmetu: '))
	dictionary[subject] = value

obj = prospech(dictionary, count)
