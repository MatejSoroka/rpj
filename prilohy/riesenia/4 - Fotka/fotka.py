# Vytvorte funkciu, ktorá zistí či na fotke je tvár,
# polia znázorňujú pixely obrázkov
def is_face_on_photo(photo):
    length = len(photo)
    arr = ["f","a","c","e"]
    cnt = 0
    if(length > 1):
        for letter in photo:
            for t in letter:
                if(t in arr):
                    cnt += 1
                    arr.remove(t)
        if(cnt == 4):
            result = True
        else:
            result = False
    else:
        result = False
    return result

# Testy:
print is_face_on_photo([['f', 'a'],['c', 'e']]) # True
print is_face_on_photo([['f', 'a', 'c', 'e']]) # False
print is_face_on_photo([['e','c','x'], ['a','f','x'],['x','x','x']]) # True
print is_face_on_photo([['f','f','x'], ['a','a','x'],['x','x','x']]) # False