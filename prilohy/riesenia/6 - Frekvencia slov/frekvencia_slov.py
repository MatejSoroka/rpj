# Vytvorte fuknciu, ktorá spočíta koľko krát sa nachádza slovo vo vete [slovo nemusi byt oddelene len medzerou]
def word_frequency(s):
	s = s.lower()
	words = {}
	add = True
	string = ""
	for letter in s:
		if letter.isalpha():
			string += letter
			last = True
		else:
			if last:
				if string in words:
					words[string] += 1
				else:
					words[string] = 1
				string = ""
				last = False
	if string in words:
		words[string] += 1
	else:
		words[string] = 1
	return words
				
# testy
print word_frequency("Som z BJ, chodim na SPS v BJ")	# {'na': 1, 'sps': 1, 'som': 1, 'bj': 2, 'chodim': 1, 'v': 1, 'z': 1}