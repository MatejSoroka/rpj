# Vytvorte funkciu, ktorá zistí či dané slovo je palindróm (číta sa rovnako z oboch strán)

def is_palindrome(string):
        length = len(string)
        count = 0
        repeats = length // 2 
        for i in range(repeats):
                if(string[length - (i + 1)] == string[i]):
                        count += 1
                        
        if(count == repeats):
                result = True
        else:
                result = False

        return result

print is_palindrome('oko'); # True
print is_palindrome('ucho'); # False
